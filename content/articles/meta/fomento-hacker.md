title: A importância de ambientes de fomento para o crescimento do movimento Hacker
date: 2020-09-26

No dia 26/09 o hackerspace feec participou de uma mesa organizada pela Semana de Engenharia Elétrica (SEE) sobre ambientes de incentivo ao movimento hacker. Segue abaixo alguns pontos conversados e o vídeo do evento disponível no youtube.

## Maker versus Hacker

Os dois termos tem bastante intersecção, o termo hacker é bastante conhecido e existe antes mesmo do surgimento da internet, enquanto que o termo maker é mais recente, mas está bastante na moda atualmente. Os dois termos se referem basicamente a comunidades ligadas a uma cultura de "faça-você-mesmo", incentivando a criação, exploração e usos convencionais ou não de tecnologias no seu sentido mais amplo. Nesses espaços em geral é incentivado a criação e o uso de tecnologias livres e abertas, como por exemplo a placa de prototipagem [Arduino](https://hackerspacefeec.gitlab.io/arduino-uno.html) ou o design brasileiro [Franzininho](https://hackerspacefeec.gitlab.io/franzininho.html).

Em ambos os movimentos existe um senso de coletividade e compartilhamento de informação e conhecimentos em geral, mas no movimento hacker isso é mais forte, pois existe um senso maior de cultura e identidade. Existe um espírito hacker propriamente dito, englobando também aspectos éticos e políticos. Em contrapartida o movimento maker se apropria de seu irmão mais antigo de forma mais utilitarista e assim, é mais facilmente adaptado por instituições hegemônicas capitalistas. A diferença portanto é menos nas atividades realizadas nos espaços (programação, eletrônica, marcenaria, arte, culinária, ...) e mais em termos de história e princípios norteadores.

Na FEEC, a escolha pelo termo hacker foi tanto uma forma de afirmar o aspecto positivo da palavra - em oposição à conotação pejorativa ligada à imagem midiática do invasor de computadores -, mas também para se diferenciar de um termo ligado estritamente ao produtivismo e à lógica do lucro.

## Software Livre versus Open Source

O debate entre os termos maker e hacker é similar a outro que existe entre open source e software livre. Da mesma forma que o termo hacker, o movimento pró software livre precede o open source e surgiu do questionamento ético da utilização de um software do qual a/o usuária/o não possui controle e portanto é controlada/o ao invés de controlar o programa. Assim, o software livre vem com a proposta de escrever programas cujo código ofereça liberdades a quem usa. Liberdade de ler o código, de estudá-lo, de modificá-lo e aperfeiçoá-lo e finalmente de redistribuí-lo para beneficiar toda a comunidade. O propósito é garantir direitos fundamentais da/os usuária/os, e incentivar que todas/os contribuam entre si.

O open source surge depois, inspirado no software livre, quando as empresas notam que manter o código aberto e estimular a contribuição entre diversos indivíduos garante uma melhor qualidade do código e diminui custos de manutenção, tornando-se assim mais lucrativo. Portanto o open source surge do oportunismo de se aproveitar os benefícios do software livre, porém desconsiderando-se os propósitos éticos e políticos que o fundamentam. As medidas são as mesmas, mas a motivação é outra: não mais pela liberdade da/os usuárias/os, mas por maiores lucros. Da mesma forma, o movimento maker se aproveita dos aspectos práticos da cultura hacker, mas sem se preocupar com as questões éticas.

## Dificuldades para a criação de um hackerspace

A primeira dificuldade para criação desses espaços seria o de desmistificar o significado da palavra hacker. Apesar de todos os esforços para refoçar o uso positivo da palavra, as mídias continuam por reproduzí-la com uma conotação pejorativa, talvez justamente por ser uma cultura que está à margem do funcionamento normal do sistema.

Com a ênfase na colaboração ao invés de competição, o incentivo em tentar entender e explorar tecnologias ao invés de ser controlado e se deixar influenciar por elas e uma organização que geralmente recusa a formalidade e a hierarquia, o espírito hacker vai na contramão do atual capitalismo de vigilância e controle. Assim, a cultura individualista e a lógica do lucro capitalistas também se mostram como empecilhos para a criação de hackerspaces e a difusão da cultura hacker.

Já dentro da Universidade, uma das dificuldades é consequência do que já foi dito. O foco em agilizar a formatura de estudantes, a ênfase em metrificações individuais, o desincentivo a trabalhos em grupo e no protagonismo de estudantes para criação de projetos, etc. fazem com que a Universidade seja um lugar difícil para a criação de um hackerspace. Como comentado em [outra mesa do evento](https://www.youtube.com/watch?v=QVd2p91V4dg), a maioria do corpo da Universidade está preocupada não em formar pessoas, mas sim, gerar mão de obra especializada para o mercado.

## Outros exemplos e como difundir a cultura hacker

Exemplos de hackerspaces importantes e mais próximos são o [Garoa](https://garoa.net.br), em São Paulo, e o [LHC](https://lhc.net.br), em Campinas. Também dentro da Unicamp existe um grupo de estudos do kernel Linux chamado [LKCAMP](https://lkcamp.gitlab.io/), que apesar de não ser um hackerspace, também é guiado pela cultura hacker: aberto para a participação de qualquer pessoa interessada, possui hierarquia horizontal (todas as pessoas podem ajudar igualmente no gerenciamento do espaço e das atividades), e foco no aprendizado e no compartilhamento de informação.

Acho que esses exemplos são importantes e mostram que, embora a cultura hacker seja uma verdadeira linha de fuga dentra da sociedade capitalista, experiências positivas estão sendo criadas e mantidas. Assim, acreditamos que esse próprio evento é uma importante forma de difusão. Se ainda não é possível mudar a estrutura toda do sistema de forma confluente aos ideias hacker, a forma de difundir essa cultura é justamente criando espaços, produzindo conteúdo e convidado mais pessoas!

## Vídeo do evento

<a href="https://www.youtube.com/watch?v=S6S78wSuc38"><img alt="Vídeo da roda de conversa no youtube" src="http://i3.ytimg.com/vi/S6S78wSuc38/maxresdefault.jpg"></a>
