#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

from pelican_jupyter import markup as nb_markup

AUTHOR = 'hackerspacefeec'
SITENAME = 'hackerspace FEEC'
DESCRIPTION = ''
SITEURL = '/'

THEME = 'theme'
# PYGMENTS_STYLE = 'monokai'
SITEIMAGE = '/theme/images/glider.jpg width=135 height=135'
RFG_FAVICONS = True

LINKS = (
        ('blog', SITEURL),
        )

MY_LINKS = (
    ("Blog", "https://hackerspacefeec.gitlab.io"),
    ('Element/matrix', 'https://matrix.to/#/!fefUMvuSJqVsEDWhvS:matrix.org?via=matrix.org'),
    ('Reunião Aberta', 'https://meet.jit.si/hackerspacefeec'),
    ('Forms sobre a sala de estudos','https://forms.gle/fC3MFFPtfyaLG1gr7')
)

ICONS = (
    ('gitlab', 'https://gitlab.com/hackerspacefeec'),
    ('comments', 'https://hackerspacefeec.gitlab.io/salas-publicas-no-element-e-no-telegram.html'),
    ('facebook', 'https://www.facebook.com/hackerspacefeec'),
    ('instagram', 'https://www.instagram.com/hackerspacefeec/'),
)

PATH = 'content'

DIRECT_TEMPLATES = ['index', 'tags', 'categories', 'authors', 'archives', 'links']


OUTPUT_PATH = 'public/'

STATIC_PATHS = [
        'images',
        'extra',
        ]

PLUGINS = [nb_markup]
MARKUP = ('md', 'ipynb')
IGNORE_FILES = ['.ipynb_checkpoints']
# IPYNB_SKIP_CSS = False
# IPYNB_EXPORT_TEMPLATE = 'theme/static/css/jupyter.tpl'

DEFAULT_DATE_FORMAT = '%d/%m/%Y'

EXTRA_PATH_METADATA = {
     'extra/favicon.ico': {'path': 'favicon.ico'},
}

TIMEZONE = 'America/Sao_Paulo'

DEFAULT_LANG = 'pt_br'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
